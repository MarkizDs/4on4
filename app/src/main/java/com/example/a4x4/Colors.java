package com.example.a4x4;

/**
 * Created by Artem on 28.02.2017.
 */
public class Colors {
	public static final int[] COLORS = {
			R.color.colorPink,
			R.color.colorBlue,
			R.color.colorOrange,
			R.color.colorRed
	};
}
