package com.example.a4x4.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Artem on 28.02.2017.
 */
public class Circle implements Parcelable {
	public int color;
	public float cx;
	public float cy;
	public float radius;
	public String squareId;

	public Circle(int color, float cx, float cy, float radius, String id) {
		this.color = color;
		this.cx = cx;
		this.cy = cy;
		this.radius = radius;
		this.squareId = id;
	}

	private Circle(Parcel in) {
		color = in.readInt();
		cx = in.readFloat();
		cy = in.readFloat();
		radius = in.readFloat();
		squareId = in.readString();
	}

	public static final Creator<Circle> CREATOR = new Creator<Circle>() {
		@Override
		public Circle createFromParcel(Parcel in) {
			return new Circle(in);
		}

		@Override
		public Circle[] newArray(int size) {
			return new Circle[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(color);
		dest.writeFloat(cx);
		dest.writeFloat(cy);
		dest.writeFloat(radius);
		dest.writeString(squareId);
	}

	public Circle copy() {
		return new Circle(color, cx, cy, radius - 10, squareId);
	}
}
