package com.example.a4x4.model;

/**
 * Created by Artem on 30.03.2017.
 */

public class User {

	private String id;
	private String userName;
	private String mac;

	public User() {
	}

	public User(String id, String name, String mac) {
		this.id = id;
		this.userName = name;
		this.mac = mac;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	@Override
	public String toString() {
		return "User{" +
				"id='" + id + '\'' +
				", userName='" + userName + '\'' +
				", mac='" + mac + '\'' +
				'}';
	}
}
