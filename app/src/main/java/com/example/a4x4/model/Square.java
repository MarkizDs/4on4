package com.example.a4x4.model;

import android.graphics.RectF;

/**
 * Created by Artem on 28.02.2017.
 */
public class Square {
	private String id;
	private RectF rect;
	private boolean isBlock;
	private boolean notStart;

	public Square(String id, RectF rect, boolean isBlock, boolean notStart) {
		this.id = id;
		this.rect = rect;
		this.notStart = notStart;
		this.isBlock = isBlock;
	}

	public boolean isNotStart() {
		return notStart;
	}

	public boolean isBlock() {
		return isBlock;
	}

	public void setRect(RectF rect) {
		this.rect = rect;
	}

	public RectF getRect() {
		return rect;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
