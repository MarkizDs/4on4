package com.example.a4x4.db;

import android.util.Log;

import com.example.a4x4.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Artem.
 */
public class StorageImpl implements Storage, ValueEventListener {

	private static final String TAG = StorageImpl.class.getSimpleName();

	public static final GenericTypeIndicator<List<User>> mapType = new GenericTypeIndicator<List<User>>() {
	};

	public static final StorageImpl self = new StorageImpl();

	private DatabaseReference usersDb;
	private DatabaseReference gamesDb;

	private List<User> mUserList;
	private List<Storage.Callback> mCallbackList;


	private StorageImpl() {
		usersDb = FirebaseDatabase.getInstance().getReference("users");
		gamesDb = FirebaseDatabase.getInstance().getReference("games");

		mCallbackList = new ArrayList<>();
	}

	@Override
	public void addUser(User user) {
		if (mUserList == null) {
			mUserList = new ArrayList<>();
		}

		mUserList.add(user);

		usersDb.setValue(mUserList);
	}

	@Override
	public void getUsers(Callback callback) {
		mCallbackList.add(callback);

		usersDb.addValueEventListener(this); // onDataChange event is triggered after listener is attached first time
	}

	@Override
	public void onDataChange(DataSnapshot dataSnapshot) {
		Log.d(TAG, "onDataChange: " + dataSnapshot.toString());

		mUserList = dataSnapshot.getValue(mapType);
		if (mUserList == null) {
			return;
		}

		Log.d(TAG, "onDataChange: " + mUserList.size());

		for (User user : mUserList) {
			if (user != null) {
				Log.d(TAG, "onDataChange: " + user);
			}
		}

		if (mCallbackList.isEmpty()) {
			return;
		}

		for (Callback callback : mCallbackList) {
			callback.onUsersReceived(mUserList);
		}
	}

	@Override
	public void onCancelled(DatabaseError databaseError) {
		Log.e(TAG, "onCancelled: " + databaseError.getMessage());
	}
}
