package com.example.a4x4.db;

import com.example.a4x4.model.User;

import java.util.List;

/**
 * @author Artem.
 */
public interface Storage {

	public interface Callback {
		void onUsersReceived(List<User> list);
	}

	void addUser(User user);

	void getUsers(Callback callback);
}
