package com.example.a4x4.utils;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;

import com.example.a4x4.model.Circle;

/**
 * @author Artem.
 */
public class MoveAnimation extends ValueAnimator
		implements ValueAnimator.AnimatorUpdateListener, ValueAnimator.AnimatorListener {

	private static final int DURATION = 300;

	private View mView;
	private Callback mCallback;
	private boolean mIsHorizontal;
	private Circle mCircle;

	public interface Callback {
		void onFinish();
	}

	public MoveAnimation(View view, Callback callback, Circle model, float to, boolean isHorizontal) {
		mView = view;
		mCircle = model;
		mCallback = callback;
		mIsHorizontal = isHorizontal;

		setDuration(DURATION);

		addListener(this);
		addUpdateListener(this);

		setFloatValues(mIsHorizontal ? mCircle.cx : mCircle.cy, to);
	}

	@Override
	public void onAnimationUpdate(ValueAnimator animation) {
		if (mIsHorizontal) {
			mCircle.cx = (float) animation.getAnimatedValue();
		} else {
			mCircle.cy = (float) animation.getAnimatedValue();
		}
		mView.invalidate();
	}

	@Override
	public void onAnimationStart(Animator animation) {

	}

	@Override
	public void onAnimationEnd(Animator animation) {
		if (mCallback != null) {
			mCallback.onFinish();
		}
	}

	@Override
	public void onAnimationCancel(Animator animation) {

	}

	@Override
	public void onAnimationRepeat(Animator animation) {

	}
}
