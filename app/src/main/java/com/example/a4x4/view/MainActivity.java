package com.example.a4x4.view;

import android.os.Bundle;

import android.support.v7.widget.RecyclerView;

import com.example.a4x4.R;

import com.example.a4x4.view.fragments.StartFragment;


public class MainActivity extends AppActivity {
	private RecyclerView recyclerView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.a_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.content, StartFragment.newInstance())
					.commit();
		}

	}
}
