package com.example.a4x4.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a4x4.R;

/**
 * Created by Artem on 01.04.2017.
 */
public class GameFragment extends BaseFragment {

	private static final String TAG = GameFragment.class.getSimpleName();

	public static Fragment newInstance() {
		Bundle args = new Bundle();

		GameFragment fragment = new GameFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.f_game, container, false);
	}
}
