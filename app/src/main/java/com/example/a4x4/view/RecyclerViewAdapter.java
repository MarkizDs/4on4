package com.example.a4x4.view;



import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a4x4.R;
import com.example.a4x4.model.User;

import java.util.ArrayList;


/**
 * Created by Artem on 06.04.2017.
 */

class RecyclerViewAdapter extends  RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
	private ArrayList<User> users;
	RecyclerViewAdapter(ArrayList<User> users) {
		this.users = users;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.f_users_list,parent,false));
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		holder.userName.setText(users.get(position).getUserName());
	}

	@Override
	public int getItemCount() {
		return users.size();
	}

	class ViewHolder extends RecyclerView.ViewHolder {
		private TextView userName;
		ViewHolder(View itemView) {
			super(itemView);
			userName = (TextView) itemView.findViewById(R.id.list_item_text);
		}
	}
}