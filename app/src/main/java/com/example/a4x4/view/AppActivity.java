package com.example.a4x4.view;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.example.a4x4.view.fragments.BaseFragment;

import java.util.List;

/**
 * @author Artem.
 */
public class AppActivity extends AppCompatActivity {

	@Override
	public void onBackPressed() {
		if (!onBackPressedUser()) super.onBackPressed();
	}

	public boolean onBackPressedUser() {
		List<Fragment> fragments = getSupportFragmentManager().getFragments();
		if (fragments != null) {
			for (Fragment fragment : fragments) {
				if (fragment instanceof BaseFragment && ((BaseFragment) fragment).dispatchOnBackPressed()) {
					return true;
				}
			}
		}
		return false;
	}
}
