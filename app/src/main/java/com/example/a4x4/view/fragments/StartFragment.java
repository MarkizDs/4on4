package com.example.a4x4.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;

import com.example.a4x4.App;
import com.example.a4x4.R;
import com.example.a4x4.view.dialogs.SettingsDialog;

/**
 * Created by Artem on 01.04.2017.
 */
public class StartFragment extends BaseFragment implements View.OnClickListener, SettingsDialog.Callback {

	private static final String TAG = StartFragment.class.getSimpleName();

	public static Fragment newInstance() {
		Bundle args = new Bundle();

		StartFragment fragment = new StartFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.f_start, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		view.findViewById(R.id.start).setOnClickListener(this);
		view.findViewById(R.id.settings).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.start) {
			getActivity().getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.content, GameFragment.newInstance())
					.addToBackStack(null)
					.commit();
		} else if (v.getId() == R.id.settings) {
			new SettingsDialog(getContext(), this).show();
		}
	}

	@Override
	public void onSettingsDialog(int id, String name) {
		switch (id) {
			case R.id.buttonNickName:
				App.getSharedPref().edit().putString("newNickName", name).apply();
				String highScore = App.getSharedPref().getString("newNickName", name);
				Log.d("NICK", "name = " + highScore);
				break;
		}
	}
}
