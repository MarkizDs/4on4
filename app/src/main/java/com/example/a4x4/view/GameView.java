package com.example.a4x4.view;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.telephony.TelephonyManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;

import com.example.a4x4.App;
import com.example.a4x4.Colors;
import com.example.a4x4.R;
import com.example.a4x4.db.Storage;
import com.example.a4x4.db.StorageImpl;
import com.example.a4x4.model.Circle;
import com.example.a4x4.model.Square;
import com.example.a4x4.model.User;
import com.example.a4x4.utils.MoveAnimation;
import com.example.a4x4.view.dialogs.GameOverDialog;
import com.example.a4x4.view.dialogs.MenuDialog;
import com.example.a4x4.view.dialogs.SettingsDialog;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Artem.
 */
public class GameView extends View implements ViewTreeObserver.OnGlobalLayoutListener,
		MoveAnimation.Callback, MenuDialog.Callback, GameOverDialog.Callback, SettingsDialog.Callback {

	private static final String TAG = GameView.class.getSimpleName();

	private static final int SIZE = 5;

	private String nickName;

	private int count = 0;
	private int highScore;

	private Paint gridPaint;
	private Paint blockPaint;
	private Paint contentPaint;
	private Paint bottomblockPaint;
	private Paint logoPaint;
	private Paint menuPaint;
	private Paint scorePaint;
	private Paint textPaint;
	private Paint textScorePaint;

	private Circle ghostCircle;
	private Circle currentCircle;

	private float squareSize;
	private float menuBarHeight;
	private float menuBarWidth;
	private float menuBarLeftWidth;
	private float menuBarLeftHeight;

	private ArrayList<Square> squareList;
	private ArrayList<Circle> circleList;

	private String[] blockOne = new String[]{"00", "01", "10", "11"};
	private String[] blockTwo = new String[]{"03", "04", "13", "14"};
	private String[] blockThree = new String[]{"31", "30", "41", "40"};
	private String[] blockFour = new String[]{"33", "34", "43", "44"};

	private String[][] blocks = new String[][]{blockOne, blockTwo, blockThree, blockFour};

	private RectF menu;
	private RectF score;
	private RectF rectLogo;
	private RectF scoreBest;
	private RectF scoreList;

	private User user = new User("1",App.getSharedPref().getString("newNickName", nickName),getMacAddress(getContext()));

	public GameView(Context context) {
		super(context);

		setId(R.id.view_id);

		init();
	}

	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);

		init();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawColor(Color.WHITE);

		if (squareList == null) {
			return;
		}

		drawTopMenu(canvas);
		drawBottomSquare(canvas);
		drawSquare(canvas);
		drawCircle(canvas);

		if (ghostCircle != null) {
			contentPaint.setColor(ghostCircle.color);
			contentPaint.setAlpha(100);
			canvas.drawCircle(ghostCircle.cx, ghostCircle.cy, ghostCircle.radius, contentPaint);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (scoreList.contains(event.getX(), event.getY())) {
					new GameOverDialog(getContext(), GameView.this, count).show();
					return true;
				}

				if (menu.contains(event.getX(), event.getY())) {
					new MenuDialog(getContext(), GameView.this).show();
					return true;
				}

				currentCircle = findCircle(event.getX(), event.getY());
				if (currentCircle != null) {
					ghostCircle = currentCircle.copy();
				}

				return true;
			case MotionEvent.ACTION_MOVE:
				if (ghostCircle != null) {
					ghostCircle.cx = event.getX();
					ghostCircle.cy = event.getY();

					invalidate();
				}
				return true;
			case MotionEvent.ACTION_UP:
				ghostCircle = null;
				invalidate();
				Square finalSquare = findSquare(event.getX(), event.getY());
				if (currentCircle == null || finalSquare == null) {
					return true;
				}
				if (finalSquare.isBlock()) {
					return true;
				}
				if (findCircle(finalSquare.getRect().centerX(), finalSquare.getRect().centerY()) != null) {
					return true;
				}
				currentCircle.squareId = finalSquare.getId();

				moveTo(finalSquare.getRect().centerX(), finalSquare.getRect().centerY());

				currentCircle = null;
				count++;
				invalidate();
				return true;
			default:
				return false;
		}
	}

	@Override
	public void onFinish() {
		if (isWin()) {
			if (highScore > count || highScore == 0) {
				App.getSharedPref().edit().putInt("newHighScore", count).apply();
			}
			new GameOverDialog(getContext(), GameView.this, count).show();
		}
	}

	@Override
	protected Parcelable onSaveInstanceState() {
		Log.d(TAG, "onSaveInstanceState: ");

		Parcelable superState = super.onSaveInstanceState();

		return new SavedState(superState, circleList, count);
	}

	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		Log.d(TAG, "onRestoreInstanceState: ");

		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}

		SavedState savedState = (SavedState) state;
		super.onRestoreInstanceState(savedState.getSuperState());

		count = savedState.count;
		circleList = savedState.circles;
	}

	@Override
	public void onGlobalLayout() {
		getViewTreeObserver().removeOnGlobalLayoutListener(this);

		squareList = new ArrayList<>();

		float w = getWidth();
		float h = getHeight();

		squareSize = Math.min(w, h) / SIZE;

		Log.d(TAG, "onGlobalLayout: w = " + w + ", h = " + h);
		float totalSquareSize = squareSize * SIZE;

		float deltaY = 0;
		float deltaX = 0;
		float maxSize = Math.max(w, h);

		menuBarHeight = h - w - (squareSize / 2);
		menuBarWidth = w;

		menuBarLeftHeight = h;
		menuBarLeftWidth = w - h - (squareSize / 2);

		int orientation = getResources().getConfiguration().orientation;

		if (maxSize > totalSquareSize) {
			if (Configuration.ORIENTATION_PORTRAIT == orientation) {
				deltaY = (maxSize - totalSquareSize) / 2;
			} else {
				deltaX = (maxSize - totalSquareSize) / 2;
			}
		}

		boolean isBlock;
		boolean notStart;

		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < SIZE; j++) {
				notStart = i == 1 && j == 2 ||
						i == 2 && j == 1 ||
						i == 3 && j == 2 ||
						i == 2 && j == 3;

				isBlock = i == 2 && j == 2 ||
						i == 0 && j == 2 ||
						i == 2 && j == 0 ||
						i == 2 && j == 4 ||
						i == 4 && j == 2;

				RectF rect = new RectF(
						i * squareSize + deltaX + (deltaX / 2),
						j * squareSize + (deltaY + (deltaY / 2)),
						i * squareSize + squareSize + deltaX + (deltaX / 2),
						j * squareSize + squareSize + deltaY + (deltaY / 2));

				squareList.add(new Square(String.valueOf(i).concat(String.valueOf(j)), rect, isBlock, notStart));
			}
		}

		Collections.shuffle(squareList);

		if (circleList != null) {
			for (Circle circle : circleList) {
				for (Square square : squareList) {
					if (square.getId().equals(circle.squareId)) {
						Log.d(TAG, "onGlobalLayout: getId " + square.getId());
						circle.cx = square.getRect().centerX();
						circle.cy = square.getRect().centerY();
						circle.radius = square.getRect().height() / 2 - getResources().getInteger(R.integer.padding);
					}
				}
			}
			invalidate();
			return;
		}

		int count = 0;
		circleList = new ArrayList<>();
		for (Square square : squareList) {
			if (!square.isNotStart() && !square.isBlock()) {
				circleList.add(new Circle(ResourcesCompat.getColor(getResources(), (Colors.COLORS[count / Colors.COLORS.length]), null),
						square.getRect().centerX(),
						square.getRect().centerY(),
						square.getRect().height() / 2 - getResources().getInteger(R.integer.padding),
						square.getId()));
				count++;
			}
		}
		invalidate();
	}

	@Override
	public void onMenuClicked(int id) {
		switch (id) {
			case R.id.newGame:
				newGame();
				break;
			case R.id.buttonSettings:
				new SettingsDialog(getContext(), this).show();
				break;
			case R.id.buttonPvP:
				StorageImpl.self.addUser(user);
				StorageImpl.self.getUsers(new Storage.Callback() {
					@Override
					public void onUsersReceived(List<User> list) {
						Log.d(TAG, "onUsersReceived: " + list.size());
					}
				});
				break;
		}
	}

	@Override
	public void onGameOverClicked(int id) {
		switch (id) {
			case R.id.newGame:
				newGame();
				break;
		}
	}

	@Override
	public void onSettingsDialog(int id, String name) {
		switch (id) {
			case R.id.buttonNickName:
				nickName = name;
				App.getSharedPref().edit().putString("newNickName", nickName).apply();
				String nick = App.getSharedPref().getString("newNickName", nickName);
				Log.d("NICK", "name = " + nick);
				break;
		}
	}

	private static class SavedState extends BaseSavedState {
		int count;
		ArrayList<Circle> circles;

		SavedState(Parcelable superState, ArrayList<Circle> list, int c) {
			super(superState);

			count = c;
			circles = list;
		}

		private SavedState(Parcel in) {
			super(in);

			count = in.readInt();
			in.readList(circles, Circle.class.getClassLoader());
		}

		@Override
		public void writeToParcel(Parcel out, int flags) {
			super.writeToParcel(out, flags);

			out.writeInt(count);
			out.writeList(circles);
		}

		//required field that makes Parcelables from a Parcel
		public static final Parcelable.Creator<SavedState> CREATOR =
				new Parcelable.Creator<SavedState>() {
					public SavedState createFromParcel(Parcel in) {
						return new SavedState(in);
					}

					public SavedState[] newArray(int size) {
						return new SavedState[size];
					}
				};
	}

	private void moveTo(float x, float y) {
		float deltaX = x - currentCircle.cx;
		float deltaY = y - currentCircle.cy;

		Log.d(TAG, "move: deltaX = " + deltaX + ", deltaY = " + deltaY);

		// just double tap
		if (deltaX == 0 && deltaY == 0) {
			return;
		}

		// winding path
		if (Math.abs(Math.round(deltaX)) == Math.abs(Math.round(deltaY))) {
			return;
		}

		// too long path
		if (Math.floor(Math.abs(deltaX)) > squareSize || Math.floor(Math.abs(deltaY)) > squareSize) {
			return;
		}

		float distance;
		boolean isHorizontal;

		if (Math.abs(deltaX) > Math.abs(deltaY)) { //move X
			isHorizontal = true;
			if (currentCircle.cx < x) { //move right
				Log.d(TAG, "move right");
				distance = currentCircle.cx + squareSize;
			} else {
				Log.d(TAG, "move left");
				distance = currentCircle.cx - squareSize;
			}
		} else { //move Y
			isHorizontal = false;
			if (currentCircle.cy < y) { //move down
				Log.d(TAG, "move down");
				distance = currentCircle.cy + squareSize;
			} else { //move up
				Log.d(TAG, "move up");
				distance = currentCircle.cy - squareSize;
			}
		}

		MoveAnimation animation = new MoveAnimation(this, this, currentCircle, distance, isHorizontal);
		animation.start();
	}

	private void drawTopMenu(Canvas canvas) {
		int orientation = getResources().getConfiguration().orientation;
		if (Configuration.ORIENTATION_PORTRAIT == orientation) {
			float x = menuBarWidth / 27;
			float y = menuBarHeight / 10;

			rectLogo = new RectF(x, y, 10 * x, 8 * y);
			score = new RectF(11 * x, y, 18 * x, 4 * y);
			scoreBest = new RectF(19 * x, y, 26 * x, 4 * y);
			menu = new RectF(11 * x, 5 * y, 18 * x, 8 * y);
			scoreList = new RectF(19 * x, 5 * y, 26 * x, 8 * y);
		} else {
			float y = menuBarLeftHeight / 27;
			float x = menuBarLeftWidth / 10;

			rectLogo = new RectF(x, y, 8 * x, 10 * y);
			score = new RectF(x, 11 * y, 8 * x, 14 * y);
			scoreBest = new RectF(x, 15 * y, 8 * x, 18 * y);
			menu = new RectF(x, 19 * y, 8 * x, 22 * y);
			scoreList = new RectF(x, 23 * y, 8 * x, 26 * y);
		}

		canvas.drawRect(rectLogo, logoPaint);
		canvas.drawRect(score, scorePaint);
		canvas.drawRect(scoreBest, scorePaint);
		canvas.drawRect(menu, menuPaint);
		canvas.drawRect(scoreList, menuPaint);

		textPaint.setTextSize((rectLogo.height() / 2));
		canvas.drawText(getContext().getString(R.string.app_name), rectLogo.centerX(), rectLogo.centerY() + (rectLogo.centerY() / 4), textPaint);

		textPaint.setTextSize(menu.height() / 2);
		canvas.drawText(getContext().getString(R.string.menu), menu.centerX(), menu.centerY() + (menu.height() / 7), textPaint);
		textPaint.setTextSize((float) (scoreList.height() / 3.2));
		canvas.drawText(getContext().getString(R.string.results), scoreList.centerX(), scoreList.centerY() + (scoreList.height() / 10), textPaint);

		textScorePaint.setTextSize(score.height() / 2);
		textPaint.setTextSize(score.height() / 2);

		canvas.drawText(getContext().getString(R.string.score), score.centerX(), score.centerY() - (score.height() / 6), textScorePaint);
		canvas.drawText(String.valueOf(count), score.centerX(), score.centerY() + (score.height() / 3), textPaint);

		textScorePaint.setTextSize(scoreBest.height() / 5);
		textPaint.setTextSize(scoreBest.height() / 2);

		canvas.drawText(getContext().getString(R.string.best_score), scoreBest.centerX(), (scoreBest.centerY() - (scoreBest.height() / 6)), textScorePaint);
		canvas.drawText(String.valueOf(highScore), scoreBest.centerX(), (scoreBest.centerY() + (scoreBest.height() / 3)), textPaint);
	}

	private void drawBottomSquare(Canvas canvas) {
		for (Square square : squareList) {
			canvas.drawRect(square.getRect(), bottomblockPaint);
		}
	}

	private void drawSquare(Canvas canvas) {
		for (Square square : squareList) {
			canvas.drawRect(square.getRect(), square.isBlock() ? blockPaint : gridPaint);
		}
	}

	private void drawCircle(Canvas canvas) {
		for (Circle circle : circleList) {
			contentPaint.setColor(circle.color);
			canvas.drawCircle(circle.cx, circle.cy, circle.radius, contentPaint);
		}
	}

	private void newGame() {
		Collections.shuffle(squareList);
		circleList.clear();
		count = 0;
		int c = 0;
		for (Square square : squareList) {
			if (!square.isNotStart() && !square.isBlock()) {
				circleList.add(new Circle(ResourcesCompat.getColor(getResources(), (Colors.COLORS[c / Colors.COLORS.length]), null),
						square.getRect().centerX(),
						square.getRect().centerY(),
						square.getRect().height() / 2 - getResources().getInteger(R.integer.padding),
						square.getId()));
				c++;
			}
		}
		highScore = App.getSharedPref().getInt("newHighScore", count);
		invalidate();
	}

	private boolean isWin() {
		int circleColor = -1;
		for (int j = 0; j < blocks.length; j++) {
			for (int i = 0; i < blocks[j].length; i++) {
				Circle c = findCircleBySquareId(blocks[j][i]);
				if (c == null) {
					return false;
				}
				if (circleColor == -1) {
					circleColor = c.color;
					continue;
				}
				if (circleColor != c.color) {
					return false;
				}
			}
			circleColor = -1;
		}

		return true;
	}

	@Nullable
	private Circle findCircle(float x, float y) {
		for (Square square : squareList) {
			if (square.getRect().contains(x, y)) {
				for (Circle circle : circleList) {
					if (square.getRect().contains(circle.cx, circle.cy)) {
						return circle;
					}
				}
			}
		}
		return null;
	}

	@Nullable
	private Circle findCircleBySquareId(String id) {
		for (Circle circle : circleList) {
			if (circle.squareId.equals(id)) {
				return circle;
			}
		}
		return null;
	}

	@Nullable
	private Square findSquare(float x, float y) {
		for (Square square : squareList) {
			if (square.getRect().contains(x, y)) {
				return square;
			}
		}
		return null;
	}

	private void init() {
		Log.d(TAG, "init");

		textScorePaint = new Paint();
		textScorePaint.setColor(ResourcesCompat.getColor(getResources(), R.color.colorGray, null));
		textScorePaint.setTextAlign(Paint.Align.CENTER);
		textScorePaint.setAntiAlias(true);

		textPaint = new Paint();
		textPaint.setColor(Color.WHITE);
		textPaint.setTextAlign(Paint.Align.CENTER);
		textPaint.setAntiAlias(true);

		blockPaint = new Paint();
		blockPaint.setColor(Color.BLACK);
		blockPaint.setAntiAlias(true);

		contentPaint = new Paint();
		contentPaint.setColor(Color.BLACK);
		contentPaint.setAntiAlias(true);

		gridPaint = new Paint();
		gridPaint.setStrokeWidth(5);
		gridPaint.setColor(ResourcesCompat.getColor(getResources(), R.color.colorBrown, null));
		gridPaint.setStyle(Paint.Style.STROKE);
		gridPaint.setAntiAlias(true);

		bottomblockPaint = new Paint();
		bottomblockPaint.setColor(ResourcesCompat.getColor(getResources(), R.color.colorGray, null));
		bottomblockPaint.setAntiAlias(true);

		logoPaint = new Paint();
		logoPaint.setColor(ResourcesCompat.getColor(getResources(), R.color.colorOrange, null));
		logoPaint.setAntiAlias(true);

		menuPaint = new Paint();
		menuPaint.setColor(ResourcesCompat.getColor(getResources(), R.color.colorBlue, null));
		menuPaint.setAntiAlias(true);

		scorePaint = new Paint();
		scorePaint.setColor(ResourcesCompat.getColor(getResources(), R.color.colorBrown, null));
		scorePaint.setAntiAlias(true);

		blockPaint = new Paint();
		blockPaint.setStrokeWidth(5);
		blockPaint.setColor(ResourcesCompat.getColor(getResources(), R.color.colorBrown, null));
		blockPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		blockPaint.setAntiAlias(true);

		highScore = App.getSharedPref().getInt("newHighScore", count);

		getViewTreeObserver().addOnGlobalLayoutListener(this);
		System.out.println("Mac "+getMacAddress(getContext()));

	}

	private String getMacAddress(Context context){
		WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		WifiInfo info = manager.getConnectionInfo();
		String address = info.getMacAddress();
		return address;
	}
}



