package com.example.a4x4.view.dialogs;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.a4x4.App;
import com.example.a4x4.R;


/**
 * Created by Artem on 01.04.2017.
 */

public class SettingsDialog implements View.OnClickListener {

	private SettingsDialog.Callback mCallback;
	private AlertDialog mDialog;
	private String nickName;
	private String nickNameEditText;

	public interface Callback {
		void onSettingsDialog(int id, String nick);
	}

	public SettingsDialog(Context context, Callback callback) {

		mCallback = callback;

		LayoutInflater inflater = LayoutInflater.from(context);

		ViewGroup main = (ViewGroup) inflater.inflate(R.layout.d_settings, null);

		nickName = ((EditText) main.findViewById(R.id.textNick)).getText().toString();

		EditText editText = (EditText) main.findViewById(R.id.textNick);

		if (App.getSharedPref().getString("newNickName", nickName) == null) {
			nickNameEditText = context.getString(R.string.nickNameSave);
		} else {
			nickNameEditText = App.getSharedPref().getString("newNickName", nickName);
		}

		editText.setText(nickNameEditText);

		main.findViewById(R.id.buttonNickName).setOnClickListener(this);

		mDialog = new AlertDialog.Builder(context)
				.setView(main)
				.setCancelable(false)
				.create();
	}

	@Override
	public void onClick(View v) {
		if (mCallback != null) {
			nickName = ((EditText) mDialog.findViewById(R.id.textNick)).getText().toString();
			mCallback.onSettingsDialog(v.getId(), nickName);
			mDialog.dismiss();
		}
	}

	public void show() {
		mDialog.show();
	}
}
