package com.example.a4x4.view.dialogs;

import android.content.Context;

import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.a4x4.R;

/**
 * Created by Artem on 28.03.2017.
 */
public class GameOverDialog implements View.OnClickListener  {

	private Callback mCallback;
	private AlertDialog mDialog;

	public interface Callback {
		void onGameOverClicked(int id);
	}

	public GameOverDialog(Context context, Callback callback, int value) {
		mCallback = callback;

		LayoutInflater inflater = LayoutInflater.from(context);
		ViewGroup main = (ViewGroup) inflater.inflate(R.layout.d_game_over, null);

		Button scoreView = (Button) main.findViewById(R.id.buttonScore);
		scoreView.setText(context.getString(R.string.score) + "\n" + value);

		main.findViewById(R.id.newGame).setOnClickListener(this);

		mDialog = new AlertDialog.Builder(context)
				.setView(main)
				.setCancelable(false)
				.create();
	}

	@Override
	public void onClick(View v) {
		if (mCallback != null) {
			mCallback.onGameOverClicked(v.getId());
			mDialog.dismiss();
		}
	}

	public void show() {
		mDialog.show();
	}
}
