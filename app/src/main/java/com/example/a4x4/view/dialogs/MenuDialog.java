package com.example.a4x4.view.dialogs;


import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a4x4.R;

/**
 * Created by Artem on 27.03.2017.
 */
public class MenuDialog implements View.OnClickListener {

	private Callback mCallback;
	private AlertDialog mDialog;

	public interface Callback {
		void onMenuClicked(int id);
	}

	public MenuDialog(Context context, Callback callback) {
		mCallback = callback;

		LayoutInflater inflater = LayoutInflater.from(context);
		ViewGroup main = (ViewGroup) inflater.inflate(R.layout.d_menu, null);

		main.findViewById(R.id.newGame).setOnClickListener(this);
		main.findViewById(R.id.continue_).setOnClickListener(this);
		main.findViewById(R.id.buttonSettings).setOnClickListener(this);
		main.findViewById(R.id.buttonPvP).setOnClickListener(this);

		mDialog = new AlertDialog.Builder(context)
				.setView(main)
				.create();
	}

	@Override
	public void onClick(View v) {
		if (mCallback != null) {
			mCallback.onMenuClicked(v.getId());
			mDialog.dismiss();
		}
	}

	public void show() {
		mDialog.show();
	}
}
