package com.example.a4x4;

import android.app.Application;
import android.content.SharedPreferences;

/**
 * @author Artem.
 */
public class App extends Application {

	private static App sSelf;

	private SharedPreferences mSharedPref;

	@Override
	public void onCreate() {
		super.onCreate();

		sSelf = this;

		mSharedPref = getSharedPreferences("settings", MODE_PRIVATE);
		//mSharedPref = getSharedPreferences("nickname", MODE_PRIVATE);

	}

	public static App getSelf() {
		return sSelf;
	}

	public static SharedPreferences getSharedPref() {
		return sSelf.mSharedPref;
	}
}
